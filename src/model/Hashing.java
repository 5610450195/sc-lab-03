package model;

public class Hashing {
	private int sum;

	public int toASCII(String str)
	{
		int sum = 0;
		for(int i=0;i<str.length();i++)
		{
			sum += (int)str.charAt(i);
		}
		this.sum = sum;
		return this.sum;
	}
	
	public int modASCII(int num)
	{
		return this.sum % num;
	}
	
}

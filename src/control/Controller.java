package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Hashing;
import view.Gui;

public class Controller {
	public ActionListener action1;
	public ActionListener action2;
	public Hashing hash = new Hashing();
	public Gui gui = new Gui();
	
	public Controller() {
		addHashingListener();
		testCase();
	}
	public static void main(String[] args) {
		new Controller();
	}
	
	public void addHashingListener() {
		action1 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {            
				gui.getResultLabel1(hash.toASCII(gui.getField1().getText()));
            }
		};
		gui.getButton1(action1); 
      
		action2 = new ActionListener() {
			public void actionPerformed(ActionEvent e) {            
				int num = Integer.parseInt(gui.getField2().getText());
				int hashCode = hash.modASCII(num);
				gui.getResultLabel2(hashCode);
          }
      };
      gui.getButton2(action2);
	}
	
	public void testCase() {
		String url1 = "http://stackoverflow.com/questions/2088016/add-a-new-line-to-the-end-of-a-jtextarea";
		String url2 = "http://stackoverflow.com/questions/13337364/how-are-hash-trees-useful?rq=1";
		String url3 = "http://stackoverflow.com/questions/16910444/hash-function-hk-k-mod-m?rq=1";
		String url4 = "http://stackoverflow.com/questions/4130936/perfect-hash-function?rq=1";
		String url5 = "http://stackoverflow.com/questions/24704899/near-perfect-hash-on-known-strings?rq=1";
		String url6 = "http://stackoverflow.com/questions/2038097/randomized-binary-search-trees?rq=1";
		String url7 = "http://stackoverflow.com/questions/19018588/how-to-hash-fixed-size-string-plus-one-integer?rq=1";
		String url8 = "http://stackoverflow.com/questions/19821061/need-help-on-hash-function-and-tradeoffs?rq=1";
		String url9 = "http://stackoverflow.com/questions/2238107/how-to-write-a-hash-function-in-c?rq=1";
		String url10 = "http://stackoverflow.com/questions/2962207/constructing-a-hash-table-hash-function?rq=1";
		String url11 = "http://stackoverflow.com/questions/4664521/understanding-hash-tables?rq=1";
		String url12 = "http://stackoverflow.com/questions/10370781/using-the-hash-function?rq=1";
		
		String[] url = {url1, url2, url3, url4, url5, url6, 
				 url7, url8, url9, url10, url11, url12};

		for (String item : url) {
			gui.getTestCase(item,hash.toASCII(item),hash.modASCII(4));
		}
	}
}

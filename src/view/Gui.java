package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Gui extends JFrame
{
	private JTextArea area;
	
	private JLabel label1;
	private JLabel label2;
	
	private JTextField txt1;
	private JTextField txt2;
	
	private JButton button1;
	private JButton button2;
	
	private JLabel resultLabel1;
	private JLabel resultLabel2;
	
	private JPanel panel1;
	private JPanel panel2;
	
	public Gui()
	{
		setTitle("Hashing Function");
	    setSize(850, 550);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setLayout(null);
		
		label1 = new JLabel("URL : ");
	    label1.setBounds(35,400,70,22);
	    label2 = new JLabel("Modulus : ");
	    label2.setBounds(35,450,70,22);
	    
	    txt1 = new JTextField();
	    txt1.setText("");	
	    txt1.setBounds(110,400,250,22);
	    txt2 = new JTextField(10);
	    txt2.setText("");	
	    txt2.setBounds(110,450,250,22);
	    
	    button1 = new JButton("Submit");
	    button1.setBounds(380,400,80,22);
	    button2 = new JButton("Submit");
	    button2.setBounds(380,450,80,22);
	    
	    resultLabel1 = new JLabel("ASCII : " );
	    resultLabel1.setBounds(480,400,160,22);
	    resultLabel2 = new JLabel("hash code : " );
	    resultLabel2.setBounds(480,450,160,22);
	    
	    area = new JTextArea("");
	    area.setBounds(15, 15, 800, 350);
	    area.setEditable(false);
	    
	    add(label1);
	    add(label2);
	    add(txt1);
	    add(txt2);
	    add(button1);
	    add(button2);
	    add(resultLabel1);  
	    add(resultLabel2);  
	    add(area);  
	    setVisible(true);
	    setResizable(false);
	}
	
	public JTextField getField1() {
		return txt1;
	}
	
	public JTextField getField2() {
		return txt2;
	}
	
	public void getButton1(ActionListener action1) {
		
		button1.addActionListener(action1);
	}
	
	public void getButton2(ActionListener action2) {
		button2.addActionListener(action2);
	}
	
	public void getResultLabel1(int ascii) {
		resultLabel1.setText("ASCII : " +ascii);
		area.append(txt1.getText()+"   ASCII : " +ascii+"   ");
	}
	
	public void getResultLabel2(int hashCode) {
		resultLabel2.setText("hash code : " + hashCode);
		area.append("hash code : "+hashCode+"\n");
	}
	
	public void getTestCase(String url,int ascii, int hashCode) {
		area.append(url+"   ASCII : "+ascii+"   hash code : "+hashCode+"\n");
	}
	
	
}
